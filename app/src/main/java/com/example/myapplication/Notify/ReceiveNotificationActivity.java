package com.example.myapplication.Notify;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Activitys.MainActivitys.MapActivity;
import com.example.myapplication.R;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import lombok.SneakyThrows;


public class ReceiveNotificationActivity extends AppCompatActivity {

    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recieve_notification);

        TextView name_ = findViewById(R.id.name);
        TextView address_ = findViewById(R.id.address);
        TextView coor = findViewById(R.id.coor);
        Button button = findViewById(R.id.buttonjump);



        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        String coor_ = getIntent().getStringExtra("coor");

        List<String> coord = Arrays.asList(coor_.split(" "));

        Double lat = Double.parseDouble(coord.get(0));
        Double longi = Double.parseDouble(coord.get(1));

        addresses = geocoder.getFromLocation(lat, longi, 1);


        String address = addresses.get(0).getAddressLine(0);

        String name = getIntent().getStringExtra("name");

        //String coor_ = getIntent().getStringExtra("coor");
        name_.setText(name);

        coor.setText("Szélesség: "+lat+ "\nHosszúság: "+ longi);
        address_.setText(address);

        button.setOnClickListener(v -> {
                Intent map = new Intent(getApplicationContext(), MapActivity.class);
                map.putExtra("lat",String.valueOf(lat));
                map.putExtra("longi",String.valueOf(longi));
                map.putExtra("name","Bajba jutott: "+name);
                startActivity(map);
        });

    }
}

