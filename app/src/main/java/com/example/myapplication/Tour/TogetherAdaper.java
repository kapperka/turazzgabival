package com.example.myapplication.Tour;

import lombok.Data;

@Data
public class TogetherAdaper {
    private String string;
    private String id;

    public TogetherAdaper(String string, String id) {
        this.string = string;
        this.id = id;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
