package com.example.myapplication.Tour;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;

import java.util.ArrayList;

public class TourAdapter extends ArrayAdapter<TogetherAdaper> {

    private final Context mContext;
    int mResource;

    public TourAdapter(@NonNull Context context, int resource, @NonNull ArrayList<TogetherAdaper> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;

    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String string = getItem(position).getString();
        String id = getItem(position).getId();

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView str = convertView.findViewById(R.id.textView1);
        TextView id_ = convertView.findViewById(R.id.textView2);

        str.setText(string);
        id_.setText(id);

        return convertView;

    }
}
