package com.example.myapplication.Tour;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database.Database;
import com.example.myapplication.Activitys.MainActivitys.MapActivity;
import com.example.myapplication.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TogetherTour extends AppCompatActivity {

    String layer;

    Button btnDatePicker, btnTimePicker, add;
    EditText txtDate, txtTime;
    TextView tourname;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int tYear, tMonth, tDay, tHour, tMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_together_tour);

        btnDatePicker = (Button) findViewById(R.id.btn_date);
        btnTimePicker = (Button) findViewById(R.id.btn_time);
        add = (Button) findViewById(R.id.add);
        txtDate = (EditText) findViewById(R.id.in_date);
        txtTime = (EditText) findViewById(R.id.in_time);
        tourname = (TextView) findViewById(R.id.tourname);

        layer = getIntent().getStringExtra("layer");

        tourname.setText(layer);

        btnDatePicker.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(TogetherTour.this,
                    (view, year, monthOfYear, dayOfMonth) -> {

                        tYear = year;
                        tMonth = monthOfYear + 1;
                        tDay = dayOfMonth;
                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        btnTimePicker.setOnClickListener(v -> {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            @SuppressLint("SetTextI18n") TimePickerDialog timePickerDialog = new TimePickerDialog(TogetherTour.this,
                    (view, hourOfDay, minute) -> {
                        tHour = hourOfDay;
                        tMinute = minute;
                        txtTime.setText(hourOfDay + ":" + minute);
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        });
        add.setOnClickListener(v -> {
            if (txtDate.length() == 0 && txtTime.length() == 0) {
                Toast.makeText(TogetherTour.this, "Nincs Dátum kijelölve", Toast.LENGTH_SHORT).show();
            } else {
                String date_ = tYear + " " + tMonth + " " + tDay + " " + tHour + " " + tMinute;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter6 = new SimpleDateFormat("yyyy MM dd HH mm");
                Date date = null;
                try {
                    date = formatter6.parse(date_);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                final Calendar c = Calendar.getInstance();
                Date now = c.getTime();

                if (now.after(date)) {
                    txtDate.setText("");
                    txtTime.setText("");
                    Toast.makeText(TogetherTour.this, "Rossz dátum", Toast.LENGTH_SHORT).show();

                } else {
                    Database.getInstance().addtogethertour(layer, date);
                    Toast.makeText(TogetherTour.this, "Túra feljegyezve ide: " + layer, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MapActivity.class));
                    overridePendingTransition(0, 0);
                }


            }

        });

    }
}