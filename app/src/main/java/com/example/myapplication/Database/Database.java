package com.example.myapplication.Database;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Activitys.MainActivitys.FriendActivity;
import com.example.myapplication.Activitys.MainActivitys.MapActivity;
import com.example.myapplication.Activitys.MainActivitys.TourActivity;
import com.example.myapplication.Tour.TogetherAdaper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.SetOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import lombok.Data;


@Data
public class Database extends AppCompatActivity {
    private CollectionReference Alluser = FirebaseFirestore.getInstance().collection("Users");
    private CollectionReference Tours = FirebaseFirestore.getInstance().collection("Tours");
    private DocumentReference MyUser;

    private String username;
    private String id;
    private String name;
    private String email;
    private String point;
    private Uri linktopicture;
    public boolean visible;

    private static Database instance;

    public Database(@NonNull GoogleSignInAccount signInAccount) {
        id = signInAccount.getId();
        name = signInAccount.getDisplayName();
        email = signInAccount.getEmail();

        linktopicture = signInAccount.getPhotoUrl();

        Map<String, String> datatosave = new HashMap<>();

        instance = this;

        MyUser = Alluser.document(Objects.requireNonNull(signInAccount.getEmail()));

        Alluser.document(Objects.requireNonNull(signInAccount.getEmail())).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Log.d("KUGI", "Document exists!");
                    amIonline(true);
                    pointset();
                } else {
                    Log.d("KUGI", "Document does not exist!");
                    datatosave.put("Id", id);
                    datatosave.put("Name", name);
                    datatosave.put("Point", "0");
                    point = "0";
                    datatosave.put("Email", email);
                    datatosave.put("Visible", String.valueOf(false));
                    visible = false;
                    datatosave.put("Username", null);
                    datatosave.put("Uri", String.valueOf(linktopicture));
                    MapActivity.getInstance().nicknameset();
                    MyUser.set(datatosave);
                    amIonline(true);
                }
            } else {
                Log.d("TAG", "Failed with: ", task.getException());
            }
        });

        MyUser.get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                Object obj = documentSnapshot.get("Visible");
                if (obj instanceof String) {
                    visible = Boolean.parseBoolean(obj.toString());
                } else if (obj instanceof Boolean) {
                    visible = (boolean) obj;
                } else {

                }
            }
        });

    }


    public void existMyUsername() {
        MyUser.get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                String nickname = documentSnapshot.getString("Username");
                setUsername_(nickname);
                if (nickname == null) {
                    MapActivity.getInstance().nicknameset();
                }
            }
        });
    }

    //Friend add procedure.

    public void getRequest(String friendName) {
        Log.d("getRequest", "emindult");
        //Firebase Users collection
        Alluser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                Map<String, Object> user = new HashMap<>();

                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                    Map<String, Object> data = documentSnapshot.getData();
                    for (Map.Entry<String, Object> objectEntry : data.entrySet()) {

                        if (objectEntry.getKey().equals("Username")) {
                            Log.d("getRequest", friendName + " " + objectEntry.getValue());
                            if (friendName.equals(objectEntry.getValue())) {
                                user = data;
                            }
                        }
                    }
                }
                String email = null;
                for (Map.Entry<String, Object> entry : user.entrySet()) {
                    if (entry.getKey().equals("Email")) {
                        email = (String) entry.getValue();
                    }
                }
                friendSet(friendName, email);
                Log.d("getRequest", friendName + " " + email);
            }
        });
    }

    public void existUsername(String friendName) {
        if (username.equals(friendName)) {
            FriendActivity.getInstance().ownName();
        } else {
            //Firebase Users collection
            FirebaseFirestore.getInstance().collection("Users").get().addOnCompleteListener(task -> {
                Map<String, Object> map = null;
                if (task.isSuccessful()) {

                    boolean exist = false;
                    Map<String, Object> user = new HashMap<>();

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        map = document.getData();
                        for (Map.Entry<String, Object> entry : map.entrySet()) {

                            if (entry.getKey().equals("Username")) {
                                Log.d("existUsername", friendName + " " + entry.getValue());
                                if (friendName.equals(entry.getValue())) {
                                    Log.d("existUsername", "megban a név, de a másikba bassza meg");
                                    exist = true;
                                    user = map;
                                }
                            }
                        }
                    }
                    if (exist) {
                        existName(friendName, user);

                    } else {
                        FriendActivity.getInstance().onFalseReslt(friendName);
                    }

                }  //Task not succesful

                assert map != null;
                map.clear();
            });
        }

    }

    public void existName(String friendName, Map<String, Object> user) {
        MyUser.collection("Friends").get()
                .addOnCompleteListener(task -> {
                    boolean exist = false;
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String name = document.getId();
                            String status = (String) document.get("Status");
                            if (friendName.equals(name)) {
                                exist = true;
                                assert status != null;
                                if (status.equals("Pending")) {
                                    FriendActivity.getInstance().friendPending(friendName);
                                }
                                if (status.equals("Friend")) {
                                    FriendActivity.getInstance().friendAlredy(friendName);
                                }
                            }
                        }

                    }
                    if (!exist) {
                        //Pending check
                        alreadyPending(friendName, user);
                        //FriendActivity.getInstance().onTrueReslt(friendName,user);
                        //existUsername(friendName);
                    }
                });
    }

    public void alreadyPending(String friendName, Map<String, Object> user) {
        MyUser.collection("Pending").get()
                .addOnCompleteListener(task -> {
                    boolean exist = false;
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String name = document.getId();
                            String status = (String) document.get("Status");
                            if (friendName.equals(name)) {
                                exist = true;
                                assert status != null;
                                if (status.equals("Pending")) {

                                    String email = null;
                                    for (Map.Entry<String, Object> entry : user.entrySet()) {
                                        if (entry.getKey().equals("Email")) {
                                            email = (String) entry.getValue();
                                        }
                                    }

                                    friendSet(friendName, email);
                                }
                            }
                        }

                    }
                    if (!exist) {
                        FriendActivity.getInstance().onTrueReslt(friendName, user);
                    }
                });
    }

    //
    public void friendSet(String friendName, String email) {

        FriendActivity.getInstance().pendingPending(friendName);

        DocumentReference MyUser_Friend = MyUser.collection("Friends").document(friendName);

        Map<String, String> datatosave = new HashMap<>();

        Task<DocumentSnapshot> documentSnapshotTask = MyUser_Friend.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                datatosave.put("Status", "Friend");
                MyUser_Friend.set(datatosave);
            }
        });

        MyUser.collection("Pending").document(friendName).delete()
                .addOnSuccessListener(aVoid -> {

                })
                .addOnFailureListener(e -> {

                });

        Map<String, String> datatosaveU = new HashMap<>();

        DocumentReference FriendUser = Alluser.document(email).collection("Friends").document(username);

        FriendUser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                datatosaveU.put("Status", "Friend");
                FriendUser.set(datatosaveU);
            }

        });

    }

    public void addFriend(String friendName, Map<String, Object> user) {

        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(Calendar.getInstance().getTime());

        DocumentReference MyUser_Friend = MyUser.collection("Friends").document(friendName);

        String email = null;


        for (Map.Entry<String, Object> entry : user.entrySet()) {

            System.out.println(entry.getKey());

            if (entry.getKey().equals("Email")) {
                email = (String) entry.getValue();
                Log.d("pina", email);
            }
            Log.d("macska", entry.getKey() + ":" + entry.getValue());
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }


        assert email != null;
        DocumentReference FriendUser =
                Alluser.document(email)
                        .collection("Pending")
                        .document(username);

        Map<String, String> datatosave = new HashMap<>();
        Map<String, String> datatosaveU = new HashMap<>();

        MyUser_Friend.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                //for (Map.Entry<String, Object> entry : user.entrySet()) {
                //    datatosave.put(entry.getKey(), (String) entry.getValue());
                //}

                datatosave.put("Status", "Pending");
                datatosave.put("Time", timeStamp);
                MyUser_Friend.set(datatosave);
            }

        });

        FriendUser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                datatosaveU.put("Status", "Pending");
                datatosaveU.put("Time", timeStamp);
                FriendUser.set(datatosaveU);
            }

        });
    }

    //Friend procedure end.

    public void setfriendslist() {

        MyUser.collection("Friends").get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ArrayList<String> list = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String status = (String) document.get("Status");

                            assert status != null;
                            if (status.equals("Friend")) {
                                list.add(document.getId());
                            }
                        }
                        FriendActivity.getInstance().friendListSet(list);
                    } else {
                        Log.d("macska", "Error getting documents: ", task.getException());
                    }
                });

    }

    public void setpendinglist() {

        MyUser.collection("Pending").get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ArrayList<String> list = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String status = (String) document.get("Status");

                            assert status != null;
                            if (status.equals("Pending")) {
                                String tmp = document.getId() + " \n " + document.get("Time");
                                list.add(tmp);
                            }

                        }
                        FriendActivity.getInstance().friendListSet(list);
                    } else {
                        Log.d("macska", "Error getting documents: ", task.getException());
                    }
                });
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------


    public static Database getInstance() {
        return instance;
    }

    //data change/add
    public void realtimeCoordinates(GeoPoint myCoordinartes) {
        Map<String, Object> data = new HashMap<>();
        data.put("Coordinates", myCoordinartes);
        MyUser.set(data, SetOptions.merge());
    }

    public void amIonline(boolean online) {
        Map<String, Object> data = new HashMap<>();
        data.put("Active", online);
        MyUser.set(data, SetOptions.merge());
    }

    public void setUsername(String nickname_) {
        setUsername_(nickname_);
        MyUser.update("Username", nickname_);
    }

    public void setUsername_(String nickname_) {
        username = nickname_;
    }

    public void exist() {
        MyUser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    amIonline(true);
                }
            }
        });
    }

    public void deleteFriend(String friendname, String friendemail) {
        MyUser.collection("Friends").document(friendname).delete()
                .addOnSuccessListener(aVoid -> {

                })
                .addOnFailureListener(e -> {

                });

        Alluser.document(friendemail).collection("Friends").document(username).delete()
                .addOnSuccessListener(aVoid -> {

                })
                .addOnFailureListener(e -> {

                });

        setfriendslist();
    }


    //friend datas
    public void setFrienddatas(String item) {

        //Firebase Users collection
        Alluser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                Map<String, Object> user = new HashMap<>();

                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                    Map<String, Object> data = documentSnapshot.getData();
                    for (Map.Entry<String, Object> objectEntry : data.entrySet()) {

                        if (objectEntry.getKey().equals("Username")) {
                            Log.d("kurva", item + " " + objectEntry.getValue());
                            if (item.equals(objectEntry.getValue())) {
                                Log.d("kurva", "megban a név");
                                user = data;
                            }
                        }
                    }
                }
                FriendActivity.getInstance().dialogDatasFriend(user, item);
                //String email = null;


            }
        });
    }

//Useractivity

    public void myvisibility() {

    }

    public void setMeVisible(boolean visiblee) {
        visible = visiblee;
        MyUser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Map<String, Object> data = new HashMap<>();
                    data.put("Visible", visiblee);

                    MyUser.set(data, SetOptions.merge());
                }
            }
        });
    }

//Mapactivity

    public void pointset() {
        MyUser.get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                Double mypoint = Double.parseDouble(Objects.requireNonNull(documentSnapshot.getString("Point")));
                point = String.valueOf(mypoint);
            }
        });

    }

    public void addPoint(Double newpoint) {
        MyUser.get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                Double mypoint = Double.parseDouble(Objects.requireNonNull(documentSnapshot.getString("Point")));
                Double allpoint = (Double) (mypoint + newpoint);
                point = String.valueOf(allpoint);
                MyUser.update("Point", String.valueOf(allpoint));

            }
        });

    }


    public void friendsList() {
        MyUser.collection("Friends").get()
                .addOnCompleteListener(task -> {
                    ArrayList<String> friends = new ArrayList<>();

                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {

                            String name = document.getId();

                            String status = (String) document.get("Status");

                            assert status != null;
                            if (status.equals("Friend")) {
                                friends.add(name);
                            }
                        }
                    }

                    for (String a : friends) {
                        friendscoordinates(a);
                    }

                });
    }


    public void friendscoordinates(String friendName) {

        //Firebase Users collection
        Alluser.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                Map<String, Object> user = new HashMap<>();

                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                    Map<String, Object> data = documentSnapshot.getData();
                    for (Map.Entry<String, Object> objectEntry : data.entrySet()) {

                        if (objectEntry.getKey().equals("Username")) {
                            //Log.d("getRequest",friendName+" "+objectEntry.getValue());
                            if (friendName.equals(objectEntry.getValue())) {
                                user = data;
                            }
                        }
                    }
                }
                GeoPoint coordinates = null;
                Boolean visible = null;
                Boolean active = null;
                for (Map.Entry<String, Object> entry : user.entrySet()) {
                    if (entry.getKey().equals("Coordinates")) {
                        coordinates = (GeoPoint) entry.getValue();
                    }
                    if (entry.getKey().equals("Visible")) {
                        visible = (Boolean) entry.getValue();
                    }
                    if (entry.getKey().equals("Active")) {
                        active = (Boolean) entry.getValue();
                    }
                }
                //friendSet(friendName,email);
                Log.d("getRequest", friendName + " " + coordinates);
                MapActivity.getInstance().showfriend(friendName, coordinates, visible, active);
            }
        });


    }


//Tour Activity ----------------------------------------------------------------------------------------------------------------------

    public void addtogethertour(String activeLayer, Date date) {
        Map<String, Object> datatosave = new HashMap<>();
        //Map<String, Object> datatosave1 = new HashMap<String, Object>();

        Log.d("kutya", String.valueOf(date));

        String ID = username + "_" + date.getTime() + "_" + activeLayer;

        DocumentReference Mytours = Tours.document(ID);

        Mytours.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                datatosave.put("Username", username);
                datatosave.put("TourName", activeLayer);
                datatosave.put("Date", date);
                Mytours.set(datatosave);


            } else {
                Log.d("kutyacica", "Failed with: ", task.getException());
            }
        });

        Map<String, String> datatosave2 = new HashMap<>();

        DocumentReference MytoursParticipants = Mytours.collection("Participants").document(username);

        MytoursParticipants.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                datatosave2.put("Date", String.valueOf(date));
                MytoursParticipants.set(datatosave2);

            } else {
                Log.d("kutyacica", "Failed with: ", task.getException());
            }
        });

    }

    public void settogetherlist() {

        Tours.get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ArrayList<TogetherAdaper> list = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Timestamp datetmp = (Timestamp) document.get("Date");

                            assert datetmp != null;
                            Date date = datetmp.toDate();

                            //now date
                            final Calendar c = Calendar.getInstance();
                            Date now = c.getTime();

                            if (now.after(date)) {
                                deletetour(document.getId());
                                Log.d("kicsicica", "Utánna van");
                            } else {
                                String tourName = (String) document.get("TourName");
                                String userName = (String) document.get("Username");
                                String tour;
                                if (username.equals(userName)) {
                                    tour = date + "\n" + tourName + "\nTe túrád!";
                                } else {
                                    tour = date + "\n" + tourName + "\n" + userName;
                                }
                                TogetherAdaper tmp = new TogetherAdaper(tour, document.getId());
                                list.add(tmp);

                                Log.d("kicsicica", "Előtte van");
                            }


                        }
                        TourActivity.getInstance().tourListSet(list);
                    } else {
                        Log.d("macska", "Error getting documents: ", task.getException());
                    }
                });

    }

    public void IalreadyApplied(String tourid, String tour) {

        CollectionReference Participants = Tours.document(tourid).collection("Participants");

        ArrayList<String> participants;
        participants = new ArrayList<>();

        Participants.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    String name = document.getId();
                    participants.add(name);
                }
            }

            String tmp = tour.split("\n")[2];

            if (tmp.equals("Te túrád!")) {

                TourActivity.getInstance().dialogDatasFriendMyself(tourid, tour, participants);

            } else {
                DocumentReference JoinTourAlready = Tours.document(tourid).collection("Participants").document(username);
                JoinTourAlready.get().addOnCompleteListener(task2 -> {
                    if (task2.isSuccessful()) {
                        DocumentSnapshot document = task2.getResult();
                        if (document.exists()) {
                            TourActivity.getInstance().dialogDatasFriendYes(tourid, tour, participants);
                        } else {
                            Log.d("TAGkutya", "Document does not exist!");
                            TourActivity.getInstance().dialogDatasFriendNot(tourid, tour, participants);
                        }
                    } else {
                        Log.d("TAG", "Failed with: ", task2.getException());
                    }
                });
            }

        });

    }

    public void jointour(String tourid) {
        Map<String, String> datatosave = new HashMap<>();

        DocumentReference JoinTour = Tours.document(tourid).collection("Participants").document(username);

        JoinTour.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Date date = new Date(System.currentTimeMillis());
                datatosave.put("Date", String.valueOf(date));
                JoinTour.set(datatosave);

            } else {
                Log.d("kutyacica", "Failed with: ", task.getException());
            }
        });

    }

    public void leavetour(String tourid) {
        Tours.document(tourid).collection("Participants").document(username).delete()
                .addOnSuccessListener(aVoid -> Log.d("eper", tourid + " " + username))
                .addOnFailureListener(e -> {

                });
    }

    public void deletetour(String tourid) {
        Tours.document(tourid).delete()
                .addOnSuccessListener(aVoid -> {

                })
                .addOnFailureListener(e -> {

                });
    }


//    public void settogetherlist() {
//
//        Tours.get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful()) {
//                            ArrayList<String> list = new ArrayList<>();
//                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                String date = (String) document.get("Date");
//                                String date_[] = date.split(" ");
//                                String tourName = (String) document.get("TourName");
//                                String userName = (String) document.get("Username");
//                                String tour = "";
//                                if(username.equals(userName)){
//                                    tour = date_[1]+" "+date_[2]+" "+date_[3]+"\n"+tourName+"\nTe túrád!";
//                                }else{
//                                    tour = date_[1]+" "+date_[2]+" "+date_[3]+"\n"+tourName+"\n"+username;
//                                }
//                                list.add(tour);
//
//                            }
//                            TourActivity.getInstance().tourListSet(list);
//                        } else {
//                            Log.d("macska", "Error getting documents: ", task.getException());
//                        }
//                    }
//                });
//
//    }
}


//    //print all of users
//    public void testuser2(){
//        FirebaseFirestore.getInstance().collection("Users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                if (task.isSuccessful()) {
//                    //List<String> list = new ArrayList<>();
//                    for (QueryDocumentSnapshot document : task.getResult()) {
//                        Log.d("macska", document.getId());
//                        Map<String, Object> map = document.getData();
//                        for (Map.Entry<String, Object> entry : map.entrySet()) {
//                            Log.d("macska", entry.getKey() + ":" + entry.getValue());
//                            //System.out.println(entry.getKey() + ":" + entry.getValue());
//                        }
//                    }
//                    //Log.d("macska", list.toString());
//                } else {
//                    //Log.d("macska", "Error getting documents: ", task.getException());
//                }
//            }
//        });
//    }


