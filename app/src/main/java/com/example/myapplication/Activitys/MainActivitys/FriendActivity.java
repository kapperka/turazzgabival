package com.example.myapplication.Activitys.MainActivitys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database.Database;
import com.example.myapplication.Dialogs.DialogAddFriend;
import com.example.myapplication.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Map;

public class FriendActivity extends AppCompatActivity {

    private Button addFriend;
    private Button friendPedding;
    private Button friends;
    private TextView friendsText;

    private ListView list;

    @SuppressLint("StaticFieldLeak")
    private static FriendActivity instance;
    Database Data;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);

        Data = MapActivity.getInstance().getData();

        instance = this;

        addFriend = findViewById(R.id.friendrecord);
        friendsText = findViewById(R.id.friends);
        friendPedding = findViewById(R.id.friendpedding_button);
        friends = findViewById(R.id.friends_button);
        friends.setVisibility(View.INVISIBLE);
        list = findViewById(R.id.list_tmp);

        //Set friend list to listview
        Toast.makeText(FriendActivity.this, "Barátok cucc", Toast.LENGTH_SHORT).show();
        Database.getInstance().setfriendslist();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.friends);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.search:
                    startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.friends:
                    return true;
                case R.id.nav:
                    startActivity(new Intent(getApplicationContext(), TourActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.map:
                    startActivity(new Intent(getApplicationContext(), MapActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.user:
                    startActivity(new Intent(getApplicationContext(), UserActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
            }
            return false;
        });
        clickListeners();
    }

    @SuppressLint("SetTextI18n")
    public void clickListeners() {
        list.setOnItemClickListener((adapterView, view, i, l) -> {
            String text = String.valueOf(friendsText.getText());

            String item = (String) adapterView.getItemAtPosition(i).toString();

            String[] Stilo = item.split(" ");

            Toast.makeText(FriendActivity.this, "Erre kattintottál: " + item, Toast.LENGTH_SHORT).show();


            if (text.equals("Barátaim")) {
                Toast.makeText(FriendActivity.this, "Lefut ez a cucc", Toast.LENGTH_SHORT).show();
                Database.getInstance().setFrienddatas(item);
            }
            if (text.equals("Függőben lévő barátok")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FriendActivity.this, R.style.MyAlertDialogStyle);
                builder.setTitle("Visszaigazolás");
                builder.setMessage("Szeretnéd hogy barátok legyetek?");
                builder.setIcon(R.drawable.navbar_together);

                builder.setPositiveButton("Igen", (dialog, which) -> {
                    Toast.makeText(FriendActivity.this, text, Toast.LENGTH_SHORT).show();
                    Database.getInstance().getRequest(Stilo[0]);

                });

                builder.setNegativeButton("Mégse", (dialog, id) -> {

                });
                builder.create();
                builder.show();
            }

        });

        addFriend.setOnClickListener(v -> {
            DialogAddFriend dialog = new DialogAddFriend();
            dialog.show(getSupportFragmentManager(), "FRIEND");
        });

        friendPedding.setOnClickListener(v -> {
            friendPedding.setVisibility(View.INVISIBLE);
            friends.setVisibility(View.VISIBLE);
            friendsText.setText("Függőben lévő barátok");
            //Set friend list to listview
            Database.getInstance().setpendinglist();

        });
        friends.setOnClickListener(v -> {
            friendPedding.setVisibility(View.VISIBLE);
            friends.setVisibility(View.INVISIBLE);
            friendsText.setText("Barátaim");
            //Set friend Pedding list to listview
            Database.getInstance().setfriendslist();

        });
    }

    public void dialogDatasFriend(Map<String, Object> user, String username) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FriendActivity.this, R.style.MyAlertDialogStyle);

        StringBuilder datas = new StringBuilder();
        String friendemail = null;

        for (Map.Entry<String, Object> entry : user.entrySet()) {
            if (entry.getKey().equals("Username") || entry.getKey().equals("Email") || entry.getKey().equals("Name")) {
                datas.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
            }

            if (entry.getKey().equals("Email")) {
                friendemail = (String) entry.getValue();
            }

            if (entry.getKey().equals("Username")) {
                builder.setTitle(entry.getValue().toString());
            }
        }

        builder.setIcon(R.drawable.navbar_user);

        builder.setMessage(datas.toString());

        String finalFriendemail = friendemail;
        builder.setPositiveButton("Barát törlése", (dialog, which) -> {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(FriendActivity.this, R.style.MyAlertDialogStyle);
            builder1.setTitle("Törlés");
            builder1.setMessage("Biztosan szeretnéd törölni a barátaid közül?");
            builder1.setIcon(R.drawable.warning);

            builder1.setPositiveButton("Igen", (dialog1, which1) -> Database.getInstance().deleteFriend(username, finalFriendemail));

            builder1.setNegativeButton("Nem", (dialog12, id) -> {

            });
            builder1.create();
            builder1.show();
        });

        builder.setNeutralButton("Mégse", (dialog, which) -> {

        });


        builder.create();
        builder.show();
    }

    public void friendListSet(ArrayList<String> list) {
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                tv.setTextColor(Color.parseColor("#000000"));

                return view;
            }
        };
        this.list.setAdapter(adapter);
    }

    public void ownName() {
        Toast.makeText(FriendActivity.this, "Magadat nem veheted fel barátnak ", Toast.LENGTH_SHORT).show();
    }

    public void friendAlredy(String friendName) {
        Toast.makeText(FriendActivity.this, "Már barátok vagytok: " + friendName, Toast.LENGTH_SHORT).show();
    }

    public void pendingPending(String friendName) {
        new android.os.Handler().postDelayed(

                () -> Database.getInstance().setpendinglist(), 1000);

        Toast.makeText(FriendActivity.this, "Már bejelölt, megtörtént " + friendName, Toast.LENGTH_LONG).show();
    }

    public void friendPending(String friendName) {
        Toast.makeText(FriendActivity.this, "Már felvetted barátnak, függőben van: " + friendName, Toast.LENGTH_LONG).show();
    }

    public void onTrueReslt(String friendName, Map<String, Object> user) {
        Toast.makeText(FriendActivity.this, "Barát kérelem elküldve: " + friendName, Toast.LENGTH_SHORT).show();
        Data.addFriend(friendName, user);
    }

    public void onFalseReslt(String name) {
        Toast.makeText(FriendActivity.this, "Nincs ilyen nevű Felhasználó: " + name, Toast.LENGTH_SHORT).show();
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------

    public static FriendActivity getInstance() {
        return instance;
    }


}