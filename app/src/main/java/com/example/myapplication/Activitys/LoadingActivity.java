package com.example.myapplication.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

public class LoadingActivity extends AppCompatActivity {
    Animation topAnim;
    Animation topAnim2;
    Animation bottomAnim;
    ImageView image;
    ImageView imageme;
    TextView logo;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_loading);

        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        topAnim2 = AnimationUtils.loadAnimation(this, R.anim.top_animation_2);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);

        image = findViewById(R.id.compass);
        imageme = findViewById(R.id.imageme);
        logo = findViewById(R.id.textView);

        image.setAnimation(topAnim2);
        imageme.setAnimation(topAnim);
        logo.setAnimation(bottomAnim);

        int SPLASH_SCREEN = 3000;
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(LoadingActivity.this, LoginActivity.class);

            startActivity(intent);
            finish();
        }, SPLASH_SCREEN);

    }

}