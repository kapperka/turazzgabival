package com.example.myapplication.Activitys.MainActivitys;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.myapplication.Activitys.LoginActivity;
import com.example.myapplication.Database.Database;
import com.example.myapplication.R;
import com.firebase.ui.auth.AuthUI;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class UserActivity extends AppCompatActivity {

    Database Data;

    private Button sing_out;
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch visible;

    @SuppressLint("StaticFieldLeak")
    private static UserActivity instance;

    @SuppressLint({"WrongViewCast", "CheckResult", "NonConstantResourceId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Data = MapActivity.getInstance().getData();

        TextView username = findViewById(R.id.username);
        username.setText(Data.getUsername());

        TextView email = findViewById(R.id.myemail);
        email.setText(Data.getEmail());

        TextView name = findViewById(R.id.myname);
        name.setText(Data.getName());

        TextView score = findViewById(R.id.myscore);
        score.setText(Data.getPoint());

        sing_out = findViewById(R.id.singout);
        visible = findViewById(R.id.visible);
        ImageView profilPicture = findViewById(R.id.imageView);

        setSwitch(Database.getInstance().visible);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.user_notfind);
        requestOptions.error(R.drawable.user_notfind);

        Glide.with(UserActivity.this)
                .load(Data.getLinktopicture())
                .apply(requestOptions)
                .into(profilPicture);


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.user);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.search:
                    startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.friends:
                    startActivity(new Intent(getApplicationContext(), FriendActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.nav:
                    startActivity(new Intent(getApplicationContext(), TourActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.map:
                    startActivity(new Intent(getApplicationContext(), MapActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.user:
                    return true;
            }
            return false;
        });
        clickListeners();

        Database.getInstance().myvisibility();

    }

    private void clickListeners() {
        //Line A
        visible.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (visible.isChecked()) {
                Toast.makeText(UserActivity.this, "Megjelenek", Toast.LENGTH_SHORT).show();

                Database.getInstance().setMeVisible(true);
            } else {
                Toast.makeText(UserActivity.this, "Nem jelenek meg", Toast.LENGTH_SHORT).show();

                Database.getInstance().setMeVisible(false);
            }
        });

        sing_out.setOnClickListener(v -> {

            AuthUI.getInstance().signOut(this);

            FirebaseAuth.getInstance().signOut();

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        });
    }

    public static UserActivity getInstance() {
        return instance;
    }

    public void setSwitch(Boolean visiblee) {
        visible.setChecked(visiblee);
    }
}