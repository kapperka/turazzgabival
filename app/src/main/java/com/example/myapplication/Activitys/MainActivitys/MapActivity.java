package com.example.myapplication.Activitys.MainActivitys;

import static android.view.View.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.Database.Database;
import com.example.myapplication.Dialogs.DialogGPS;
import com.example.myapplication.Dialogs.DialogNickname;
import com.example.myapplication.Hikingplaces.HikingPlace;
import com.example.myapplication.Hikingplaces.HikingPlaces;
import com.example.myapplication.R;
import com.example.myapplication.Tour.TogetherTour;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener, SensorEventListener {

    //upload

    //Sensors
    SensorManager sensorManager;
    boolean running = false;
    boolean myselfRunning = false;

    //Notify

    private RequestQueue mRequestQue;
    private final String URL = "https://fcm.googleapis.com/fcm/send";

    //Google account

    GoogleSignInAccount signInAccount;

    private static final int REQUEST_CHECK_SETTINGS = 1001;

    //Mapbox
    private MapView mapView;
    private MapboxMap mapboxMap;

    private ArrayList<String> layers;
    private String activeLayer;

    //List items
    private ArrayList<String> hikingplaces;
    private ArrayList<HikingPlace> hikingplaces_2;
    private ArrayList<String> listItems;
    private ArrayAdapter<String> adapter;

    //List of hiking trials
    HikingPlaces Hiking;

    //
    private TextView step_counter;
    private TextView step_text;
    private TextView tourname;
    private ListView listView;
    private EditText editText;

    private Button sos;
    private Button togetherset;
    private Button myselfstart;
    private Button myselfend;

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch friendlayout;
    private AppCompatImageButton my_location;

    //steps
    int step = 0;

    //database
    private Database Data;

    //Location
    private PermissionsManager permissionsManager;
    // Variables needed to add the location engine
    private LocationEngine locationEngine;
    private final TerkepActivityLocationCallback callback = new TerkepActivityLocationCallback(this);

    @SuppressLint("StaticFieldLeak")
    private static MapActivity instance;

    private Chronometer chronometer_;

    private BottomNavigationView bottomNavigationView;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Hiking = new HikingPlaces(this);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        layers = new ArrayList<>();

        activeLayer = "none";

        signInAccount = GoogleSignIn.getLastSignedInAccount(this);

        assert signInAccount != null;
        Data = new Database(signInAccount);
        Data.existMyUsername();

        instance = this;

        //Notify
        mRequestQue = Volley.newRequestQueue(this);
        FirebaseMessaging.getInstance().subscribeToTopic("news");

        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));

        setContentView(R.layout.activity_map);

        mapView = findViewById(R.id.mapView);
        editText = findViewById(R.id.txtsearch);
        listView = findViewById(R.id.listview);

        sos = findViewById(R.id.sos);
        togetherset = findViewById(R.id.together);
        myselfstart = findViewById(R.id.myselfstart);
        myselfend = findViewById(R.id.myselfend);
        step_counter = findViewById(R.id.stepcount);
        step_text = findViewById(R.id.steptext);
        step_counter.setText(String.valueOf(step));
        tourname = findViewById(R.id.tourname);
        my_location = findViewById(R.id.mylocation);
        friendlayout = findViewById(R.id.friendlayout);

        //mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        //Stopper
        chronometer_ = findViewById(R.id.chronometer);
        chronometer_.setFormat("Idő: %s");


        bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.map);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.search:
                    startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.friends:
                    startActivity(new Intent(getApplicationContext(), FriendActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.nav:
                    startActivity(new Intent(getApplicationContext(), TourActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.map:
                    return true;
                case R.id.user:
                    startActivity(new Intent(getApplicationContext(), UserActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
            }
            return false;
        });

        clickListeners();

    }


    //Start Mapbox style
    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {


        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(new Style.Builder().fromUri("mapbox://styles/kapper56/ckm4yu0nfdl6j17l9o98aiwfd"),
                style -> {
                    enableLocationComponent(style);
                    UiSettings uiSettings = mapboxMap.getUiSettings();
                    uiSettings.setCompassGravity(Gravity.BOTTOM | Gravity.END);
                    uiSettings.setCompassMargins(0, 0, 25, 300);
                    uiSettings.setAttributionEnabled(false);
                    uiSettings.setLogoGravity(Gravity.END);
                    uiSettings.setLogoEnabled(false);
                    sos();
                });

        mapboxMap.setOnMarkerClickListener(marker -> {
            String[] helper = marker.getTitle().split(" ");
            String help = helper[0];

            if (!help.equals("Bajba") && !help.equals("Barát")) {
                toggleLayer(marker.getTitle(), false);
            }
            return false;
        });

        mapboxMap.addOnMapClickListener(point -> {

            closeKeyboard();
            friendlayout.setVisibility(View.VISIBLE);
            listView.setVisibility(View.INVISIBLE);
            myselfstart.setVisibility(View.INVISIBLE);
            togetherset.setVisibility(View.INVISIBLE);
            tourname.setVisibility(View.INVISIBLE);
            return true;

        });
    }

    public void setHikingplaces_2(ArrayList<HikingPlace> tmp) {
        this.hikingplaces_2 = tmp;
    }

    public void jump() {
        //Ha rákattintottunk egy útvonalra amire rászürtünk, akkor ez akkor fut csak le, és odadob minket arra a túraútra
        String name = getIntent().getStringExtra("Name");
        if (name != null) {
            List<com.mapbox.geojson.Point> coor = Hiking.getcoor(name);
            double lat = coor.get(0).latitude();
            double longi = coor.get(0).longitude();
            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(lat, longi))
                    .zoom(12)
                    .tilt(0)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 4000);
        }
    }

    public void routes(ArrayList<HikingPlace> hikingplaces) {
        mapboxMap.getStyle(style -> setroutes(style, hikingplaces));
    }

    private void sos() {
        String lat = getIntent().getStringExtra("lat");
        String longi = getIntent().getStringExtra("longi");
        String name = getIntent().getStringExtra("name");
        if (lat != null) {
            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(lat), Double.parseDouble(longi)))
                    .zoom(12)
                    .tilt(0)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 4000);

            mapboxMap.getStyle(style -> {

            });

            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(longi)))
                    .title(name)
                    .icon(IconFactory.getInstance(this).fromResource(R.drawable.alert32))
            );
        }
    }


    public void clickListeners() {
        //Line A
        friendlayout.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (friendlayout.isChecked()) {

                Database.getInstance().friendsList();

                Toast.makeText(MapActivity.this, "Barátok megjelennek", Toast.LENGTH_SHORT).show();
            } else {

                mapboxMap.clear();

                mapboxMap.getStyle(style -> setroutes_markers(hikingplaces_2));

                Toast.makeText(MapActivity.this, "Barátok ne jelenjenek meg", Toast.LENGTH_SHORT).show();
            }
        });
        sos.setOnClickListener(v -> {
            try {
                sendNotification();
            } catch (NullPointerException e) {
                DialogGPS dialog = new DialogGPS();
                dialog.show(getSupportFragmentManager(), "GPS");
            }


        });

        myselfstart.setOnClickListener(v -> {
            myselfStart();
            step = 0;
        });

        myselfend.setOnClickListener(v -> myselfEnd());

        togetherset.setOnClickListener(v -> {
            Intent map = new Intent(getApplicationContext(), TogetherTour.class);
            map.putExtra("layer", activeLayer);
            overridePendingTransition(0, 0);
            startActivity(map);

        });


        //Listview Click
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            String item = ((String) adapterView.getItemAtPosition(i)).split("\n")[0];
            Toast.makeText(MapActivity.this, "Erre kattintottál: " + item, Toast.LENGTH_SHORT).show();
            listView.setVisibility(View.INVISIBLE);
            closeKeyboard();
            toggleLayer(item, true);
        });

        //Text viewers
        editText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                listView.setVisibility(VISIBLE);
                try {
                    initList();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        //Text viewers
        editText.setOnClickListener(v -> {
            listView.setVisibility(VISIBLE);
            try {
                initList();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });

        //Text viewers
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listView.setVisibility(VISIBLE);
                try {
                    initList();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    String search = s.toString();
                    search = search.substring(0, 1).toUpperCase() + search.substring(1).toLowerCase();
                    searchItem(search);
                } catch (StringIndexOutOfBoundsException e) {
                    searchItem(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        my_location.setOnClickListener(v -> {
            try {
                location();
            } catch (AssertionError a) {
                DialogGPS dialog = new DialogGPS();
                dialog.show(getSupportFragmentManager(), "GPS");
            }
        });
    }

    //show friends
    public void showfriend(String friendName, GeoPoint coordinates, Boolean visible, Boolean active) {
        if (visible.equals(true) && active.equals(true))
            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(coordinates.getLatitude(), coordinates.getLongitude()))
                    .title("Barát " + friendName)
                    .icon(IconFactory.getInstance(this).fromResource(R.drawable.person32))
            );
    }

    //myselftour start
    private void myselfStart() {
        myselfRunning = true;
        running = true;

        friendlayout.setVisibility(View.INVISIBLE);
        step_counter.setVisibility(VISIBLE);
        step_text.setVisibility(VISIBLE);
        bottomNavigationView.setVisibility(View.INVISIBLE);
        chronometer_.setVisibility(View.VISIBLE);
        myselfstart.setVisibility(View.INVISIBLE);
        myselfend.setVisibility(View.VISIBLE);
        togetherset.setVisibility(View.INVISIBLE);
        tourname.setVisibility(View.INVISIBLE);


        chronometer_.setBase(SystemClock.elapsedRealtime());
        //chronometer_.setBase(SystemClock.elapsedRealtime() - (59* 60000 + 40 * 1000));

        chronometer_.start();
    }

    //myself tour end
    private void myselfEnd() {
        myselfRunning = false;
        running = false;

        friendlayout.setVisibility(View.VISIBLE);
        step_counter.setVisibility(View.INVISIBLE);
        step_text.setVisibility(View.INVISIBLE);
        bottomNavigationView.setVisibility(View.VISIBLE);
        chronometer_.setVisibility(View.INVISIBLE);
        myselfend.setVisibility(View.INVISIBLE);

        CharSequence tmp = chronometer_.getContentDescription();
        String[] times = tmp.toString().split(" ");

        double mytimesec = 0;

        for (int i = 0; i < times.length; i++) {

            if (checkNumberWithJava(times[i])) {
                switch (times[i + 1]) {
                    case "óra":
                    case "óra,":
                    case "hour":
                    case "hour,":
                        mytimesec += (Integer.parseInt(times[i]) * 3600);
                        break;
                    case "perc":
                    case "minute":
                        mytimesec += (Integer.parseInt(times[i]) * 60);
                        break;
                    case "másodperc":
                    case "seconds":
                        mytimesec += (Integer.parseInt(times[i]));
                        break;
                }
            }
        }



        //double steps = Double.parseDouble(step_counter.getText().toString());
        double steps = 30.0;

        toggleLayer(activeLayer, false);

        double mydistancekm = steps / 1312.33595801;
        //double mydistancekm = 60 / 1312.33595801;

        //1km -> 12 perc átlag

        double mytimeminute = mytimesec / 60.0;
        //Double mydistancekm

        double avg = mydistancekm * 12.0;
        double percent = (avg / mytimeminute);


        Double mypoint = (mydistancekm * 100) * percent;

        Toast.makeText(this, "Pont: " + mypoint, Toast.LENGTH_SHORT).show();

        Database.getInstance().addPoint(mypoint);

        chronometer_.stop();
    }

    public static boolean checkNumberWithJava(String text) {
        try {
            Float.parseFloat(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    //display selected/clicked layer
    private void toggleLayer(String title, boolean camera) {
        if (camera) {
            List<Point> coordinates = Hiking.getselectedcoordinates(title);
            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(coordinates.get(0).latitude(), coordinates.get(0).longitude()))
                    .zoom(12)
                    .tilt(0)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 4000);

        } else if (!myselfRunning) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                Layer nowLayer;
                Layer afterLayer;

                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    nowLayer = style.getLayer(activeLayer);

                    afterLayer = style.getLayer(title);


                    if (nowLayer != null) {
                        if (Objects.equals(nowLayer.getVisibility().getValue(), "visible")) {
                            nowLayer.setProperties(visibility(NONE));
                        }
                        if (nowLayer != afterLayer) {
                            afterLayer.setProperties(visibility(Property.VISIBLE));
                            activeLayer = title;

                            tourname.setVisibility(View.VISIBLE);
                            tourname.setText(activeLayer);

                            friendlayout.setVisibility(View.INVISIBLE);
                            myselfstart.setVisibility(View.VISIBLE);
                            togetherset.setVisibility(View.VISIBLE);
                        } else {
                            activeLayer = "none";

                            friendlayout.setVisibility(View.VISIBLE);
                            tourname.setVisibility(View.INVISIBLE);
                            togetherset.setVisibility(View.INVISIBLE);
                            myselfstart.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        assert afterLayer != null;
                        afterLayer.setProperties(visibility(Property.VISIBLE));
                        activeLayer = title;

                        friendlayout.setVisibility(View.INVISIBLE);
                        myselfstart.setVisibility(View.VISIBLE);
                        togetherset.setVisibility(View.VISIBLE);
                        tourname.setVisibility(View.VISIBLE);
                        tourname.setText(activeLayer);
                    }
                }
            });
        }
    }

    public void setroutes_markers(ArrayList<HikingPlace> hikingplacess) {
        for (HikingPlace places : hikingplacess) {

            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(places.getCoordinates().get(0).latitude(), places.getCoordinates().get(0).longitude()))
                    .title(places.getName())
            );
        }


    }

    public void setroutes(Style style, ArrayList<HikingPlace> hikingplacess) {
        for (HikingPlace places : hikingplacess) {
            GeoJsonSource marker = new GeoJsonSource(places.getName(),
                    FeatureCollection.fromFeatures(new Feature[]{Feature.fromGeometry(LineString.fromLngLats(places.getCoordinates()))}));

            style.addSource(marker);

            LineLayer route = new LineLayer(places.getName(), places.getName());

            layers.add(places.getName());

            route.setProperties(
                    PropertyFactory.lineDasharray(new Float[] {0.01f, 2f}),
                    visibility(NONE),
                    lineCap(Property.LINE_CAP_ROUND),
                    lineJoin(Property.LINE_JOIN_ROUND),
                    lineWidth(3f),
                    lineColor(Color.parseColor("#CC3610"))
            );
            style.addLayer(route);

            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(places.getCoordinates().get(0).latitude(), places.getCoordinates().get(0).longitude()))
                    .title(places.getName())
            );
        }


    }


    public void nicknameset() {
        DialogNickname dialog = new DialogNickname();
        dialog.show(getSupportFragmentManager(), "NICK");
    }


    private void sendNotification() {
        JSONObject json = new JSONObject();
        try {
            json.put("to", "/topics/" + "news");
            JSONObject notificationObj = new JSONObject();
            notificationObj.put("title", "Bajban vagyok!");
            notificationObj.put("body", "Segíts Légyszíves!");

            JSONObject extraData = new JSONObject();
            Location lastKnownLocation = mapboxMap.getLocationComponent().getLastKnownLocation();

            extraData.put("name", signInAccount.getDisplayName());
            extraData.put("email", signInAccount.getEmail());

            extraData.put("coor", lastKnownLocation.getLatitude() + " " + lastKnownLocation.getLongitude());

            json.put("notification", notificationObj);
            json.put("data", extraData);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                    json,
                    response -> Log.d("MUR", "onResponse: "), error -> Log.d("MUR", "onError: " + error.networkResponse)
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAA9_Tljm8:APA91bF-yyzy2gtR7YkrsO4c1xwt4TtS3y-EOFnNQ6hI5HcTYEb0chJjG__eWQ_jLKe_7fKMZhR39qDNjAYc1hqioBSJZ57aww_l8KzyEaHmTJq_dhkaMsNIU4lwkJV9xf48H_jOQcch");
                    return header;
                }
            };
            mRequestQue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public Database getData() {
        return Data;
    }

    public static MapActivity getInstance() {
        return instance;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Toast.makeText(this, "változik", Toast.LENGTH_SHORT).show();
        if (running) {
            step_counter.setText(String.valueOf(step));
            step += 1;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    //list hiking
    public void initList() throws UnsupportedEncodingException {
        hikingplaces = new ArrayList<>();
        for (HikingPlace tmp : Hiking.getHikingplaces()) {
            String place = tmp.getName();
            String distance = tmp.getDistance();
            String area = tmp.getArea();
            String difficulty = tmp.getDifficulty();
            hikingplaces.add(place + "\n Hossz: " + distance + "\n Nehézség: " + difficulty + "\n Hegység: " + area);
        }
        listItems = (ArrayList<String>) hikingplaces.clone();

        adapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.txtitem, listItems);
        listView.setAdapter(adapter);

    }


    //search item
    public void searchItem(String textToSearch) {
        for (String item : hikingplaces) {
            String[] tmp = item.split("\n");
            if (!tmp[0].contains(textToSearch)) {
                listItems.remove(item);
            }
        }
        adapter.notifyDataSetChanged();
    }

    //Button, my location
    public void location() {
        Location lastKnownLocation = mapboxMap.getLocationComponent().getLastKnownLocation();
        assert lastKnownLocation != null;
        CameraPosition position = new CameraPosition.Builder().target(new LatLng(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())))
                .zoom(15).tilt(0).build();
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 3000);
    }

    //Keyboard close
    private void closeKeyboard() {
        View v = this.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    //Permissions, localization
    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {

        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

            // Set the LocationComponent activation options
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();

            // Activate with the LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(locationComponentActivationOptions);

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

            // Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);

            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    //-||-

    /**
     * Set up the LocationEngine and the parameters for querying the device's location
     */
    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
        long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(request, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    //-||-
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //-||-
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    //-||-
    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
                //step permission
                steppermissions();
            }
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    //step permission
    public void steppermissions() {
        if (ContextCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MapActivity.this, Manifest.permission.ACTIVITY_RECOGNITION)) {

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACTIVITY_RECOGNITION}, 1);
            }
        }
    }


    //Turn on GPS
    public void GPS() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(2000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addAllLocationRequests(Collections.singleton(locationRequest));
        builder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getApplicationContext())
                .checkLocationSettings(builder.build());

        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);

            } catch (ApiException e) {
                switch (e.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                            resolvableApiException.startResolutionForResult(MapActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendIntentException) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;

                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();

        //Data.amIonline(true);
    }

    //Sensor
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        //running = true;
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_NORMAL);
            //Toast.makeText(this, "Sensor hozzálapcsolva", Toast.LENGTH_SHORT).show();
        }//Toast.makeText(this, "Sensor nem található", Toast.LENGTH_SHORT).show();


        Data.exist();


    }

    //Sensor
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        running = false;
        //if you unregisted the hardvare will stops detechting steps
        sensorManager.unregisterListener(this);

        Data.amIonline(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();

        Data.amIonline(false);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {

        Data.amIonline(false);

        super.onDestroy();
        // Prevent leaks
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
        mapView.onDestroy();


    }

    public HikingPlaces getHikingPlaces() {
        return Hiking;
    }


    private static class TerkepActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<MapActivity> activityWeakReference;

        TerkepActivityLocationCallback(MapActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        /**
         * The LocationEngineCallback interface's method which fires when the device's location has changed.
         *
         * @param result the LocationEngineResult object which has the last known location within it.
         */
        @SuppressLint("StringFormatInvalid")
        @Override
        public void onSuccess(LocationEngineResult result) {
            MapActivity activity = activityWeakReference.get();
            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                GeoPoint myCoordinartes = new GeoPoint(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude());

                Database data = MapActivity.getInstance().getData();

                data.realtimeCoordinates(myCoordinartes);

                // Pass the new location to the Maps SDK's LocationComponent
                if (activity.mapboxMap != null && result.getLastLocation() != null) {
                    activity.mapboxMap.getLocationComponent().forceLocationUpdate(result.getLastLocation());
                }
            }
        }

        /**
         * The LocationEngineCallback interface's method which fires when the device's location can not be captured
         *
         * @param exception the exception message
         */
        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            MapActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}

