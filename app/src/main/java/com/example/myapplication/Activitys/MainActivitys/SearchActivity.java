package com.example.myapplication.Activitys.MainActivitys;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Hikingplaces.HikingPlace;
import com.example.myapplication.Hikingplaces.HikingPlaces;
import com.example.myapplication.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    private Spinner hegyseg;
    private Spinner nehezseg;
    private Spinner tavolsag;
    private Spinner szint;
    private Spinner tulaj1;
    private Spinner tulaj2;
    private Spinner tulaj3;
    private ListView list;

    HikingPlaces Hiking;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //HikingPlaces hikingPlaces = new HikingPlaces(this, false);
        HikingPlaces hikingPlaces = MapActivity.getInstance().getHikingPlaces();
        setHikingPlaces(hikingPlaces);

        hegyseg = findViewById(R.id.spinner1);
        nehezseg = findViewById(R.id.spinner2);
        tavolsag = findViewById(R.id.spinner3);
        szint = findViewById(R.id.spinner4);
        tulaj1 = findViewById(R.id.spinner5);
        tulaj2 = findViewById(R.id.spinner6);
        tulaj3 = findViewById(R.id.spinner7);
        Button gomb = findViewById(R.id.talalatok);
        list = findViewById(R.id.list_tmp);

        feltolt();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.search);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.search:
                    return true;
                case R.id.friends:
                    startActivity(new Intent(getApplicationContext(), FriendActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.nav:
                    startActivity(new Intent(getApplicationContext(), TourActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.map:
                    startActivity(new Intent(getApplicationContext(), MapActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.user:
                    startActivity(new Intent(getApplicationContext(), UserActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
            }
            return false;
        });

        list.setOnItemClickListener((adapterView, view, i, l) -> {
            Toast.makeText(SearchActivity.this, "Erre kattintottál: " + adapterView.getItemAtPosition(i), Toast.LENGTH_SHORT).show();
            String item = (String) adapterView.getItemAtPosition(i);

            String match = item.split("(Név:\\s)|(\\s*\nHegység:)")[1];

            //startActivity(new Intent(getApplicationContext(),MapActivity.class));
            //overridePendingTransition(0,0);

            Intent map = new Intent(getApplicationContext(), MapActivity.class);
            map.putExtra("Name", match);
            overridePendingTransition(0, 0);
            startActivity(map);
        });

        gomb.setOnClickListener(v -> {
            try {
                run();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }

        });
    }

    private void run() throws CloneNotSupportedException {
        String hegy = (String) hegyseg.getSelectedItem();
        String nehez = (String) nehezseg.getSelectedItem();
        String tavol = (String) tavolsag.getSelectedItem();
        String szint_ = (String) szint.getSelectedItem();
        String tulajdon1 = (String) tulaj1.getSelectedItem();
        String tulajdon2 = (String) tulaj2.getSelectedItem();
        String tulajdon3 = (String) tulaj3.getSelectedItem();

        String[] attributes = new String[]{hegy, nehez, tavol, szint_, tulajdon1, tulajdon2, tulajdon3};

        ArrayList asd = valogat(attributes);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, asd) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                tv.setTextColor(Color.parseColor("#FFFFEAC3"));

                return view;
            }
        };
        list.setAdapter(adapter);
    }

    private ArrayList valogat(String[] attributes) {

        ArrayList yourlist = new ArrayList();

        for (HikingPlace place : Hiking.getHikingplaces()) {
            boolean good = false;

            System.out.println(place.getName() + place.getDistance());

            if ((place.getArea().equals(attributes[0]) || "Mindegy".equals(attributes[0]))
                    && (place.getDifficulty().equals(attributes[1]) || "Mindegy".equals(attributes[1]))
                    && (place.getDistancecategory().equals(attributes[2]) || "Mindegy".equals(attributes[2]))
                    && (place.getDiferencecategory().equals(attributes[3]) || "Mindegy".equals(attributes[3]))
                    && (place.include(attributes[4]) || "Mindegy".equals(attributes[4]))
                    && (place.include(attributes[5]) || "Mindegy".equals(attributes[5]))
                    && (place.include(attributes[6]) || "Mindegy".equals(attributes[6]))) {
                good = true;
            }
            if (good) {
                yourlist.add("Név: " + place.getName() + "\n" +
                        "Hegység: " + place.getArea() + "\n" +
                        "Nehézség: " + place.getDifficulty() + "\n" +
                        "Szintkülönbség: " + place.getDifference() + "\n" +
                        "Távolság: " + place.getDistance()
                );
            }
        }
        return yourlist;
    }

    public void setHikingPlaces(HikingPlaces storage) {
        this.Hiking = storage;
    }

    void feltolt() {
        String[] hegy = new String[]{"Mindegy", "Aggteleki-karszt", "Badacsony", "Bakony", "Balaton-felvidék", "Balaton-medence", "Budai-hegység", "Börzsöny", "Bükk", "Cserhát", "Cserehát", "Gerecse", "Karancs", "Karancs-Medves", "Keszthelyi-hegység", "Kőszegi-hegység", "Mecsek", "Mátra", "Pilis", "Pilis, Budai-hegység", "Soproni-hegység", "Velencei-hegység", "Villányi-hegység", "Visegrádi-hegység", "Vértes", "Vértes, Gerecse", "Zala, Őrség", "Zemplén", "Őrség"};
        String[] nehez = new String[]{"Mindegy", "1", "2", "3", "4", "5"};
        String[] tav = new String[]{"Mindegy", "1-5 km", "5-10 km", "10-15 km", "15-20 km", "20-25 km", "25-30 km", "30-35 km", "35-40 km", "40-45 km"};
        String[] tulaj = new String[]{"Mindegy", "Barlang", "Babakocsival", "Családdal", "Dombos terep", "Forrás", "Gyerekbarát", "Jól jelzett túra", "Kilátó", "Kezdő", "Kéktúra", "Kilátó", "Kőfejtő", "Közepesen jelzett túra", "Látogatóközpont", "Meleg étel", "Meredek terep", "Nyomós kút", "Nagy esőzés után nehezen járható", "Nagy melegben nem ajánlott", "Ösvény", "Profi", "Rosszul jelzett túra", "Szálláslehetőség", "Szép kilátás", "Sziklás", "Sík terep", "Tanösvény", "Túrabottal ajánlott", "Turistaház", "Tűzrakási lehetőség"};
        String[] szintkulonbseg = new String[]{"Mindegy", "0-250 m", "250-500 m", "500-750 m", "750-1000 m", "1000-1250 m", "1250-1500 m", "1500-1750 m"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, hegy);
        hegyseg.setAdapter(adapter1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, nehez);
        nehezseg.setAdapter(adapter2);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tav);
        tavolsag.setAdapter(adapter3);
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, szintkulonbseg);
        szint.setAdapter(adapter4);
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tulaj);
        tulaj1.setAdapter(adapter5);
        tulaj2.setAdapter(adapter5);
        tulaj3.setAdapter(adapter5);
    }

}