package com.example.myapplication.Activitys.MainActivitys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database.Database;
import com.example.myapplication.R;
import com.example.myapplication.Tour.TogetherAdaper;
import com.example.myapplication.Tour.TourAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class TourActivity extends AppCompatActivity {

    private ListView list;

    @SuppressLint("StaticFieldLeak")
    private static TourActivity instance;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);

        instance = this;

        list = findViewById(R.id.list_tmp);

        Database.getInstance().settogetherlist();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.nav);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.search:
                    startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.friends:
                    startActivity(new Intent(getApplicationContext(), FriendActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.nav:
                    return true;
                case R.id.map:
                    startActivity(new Intent(getApplicationContext(), MapActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
                case R.id.user:
                    startActivity(new Intent(getApplicationContext(), UserActivity.class));
                    overridePendingTransition(0, 0);
                    return true;
            }
            return false;
        });

        list.setOnItemClickListener((adapterView, view, i, l) -> {
            //Toast.makeText(TourActivity.this, "Erre kattintottál: " + adapterView.getItemAtPosition(i), Toast.LENGTH_SHORT).show();


            TogetherAdaper tmp = (TogetherAdaper) adapterView.getItemAtPosition(i);

            String tourid = tmp.getId();
            String tour = tmp.getString();

            Toast.makeText(TourActivity.this, tourid, Toast.LENGTH_SHORT).show();

            Database.getInstance().IalreadyApplied(tourid, tour);

        });

    }

    public void dialogDatasFriendMyself(String tourid, String tour, ArrayList<String> participants) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TourActivity.this, R.style.MyAlertDialogStyle);

        String[] tourdatas = tour.split("\n");
        //date, place, user

        builder.setTitle(tourdatas[1]);
        builder.setIcon(R.drawable.together_tour_clock);
        String datas = "Ez a te túrád\n\nAkik már jelentkeztek:\n";

        for (String name : participants) {
            datas += name + "\n";
        }

        builder.setPositiveButton("Túra törlése", (dialog, which) -> {
            Database.getInstance().deletetour(tourid);
            Database.getInstance().settogetherlist();
        });

        builder.setNeutralButton("Mégse", (dialog, which) -> {
        });

        builder.setMessage(datas);
        builder.create();
        builder.show();
    }


    public void dialogDatasFriendYes(String tourid, String tour, ArrayList<String> participants) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TourActivity.this, R.style.MyAlertDialogStyle);

        String[] tourdatas = tour.split("\n");
        //date, place, user

        builder.setTitle(tourdatas[1]);
        builder.setIcon(R.drawable.together_tour_clock);
        String datas = "Már jelentkeztél a túrára\n\nAkik már jelentkeztek:\n";

        for (String name : participants) {
            datas += name + "\n";
        }

        builder.setPositiveButton("Lejelentkezés a túráról", (dialog, which) -> Database.getInstance().leavetour(tourid));

        builder.setNeutralButton("Mégse", (dialog, which) -> {
        });

        builder.setMessage(datas);
        builder.create();
        builder.show();
    }

    public void dialogDatasFriendNot(String tourid, String tour, ArrayList<String> participants) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TourActivity.this, R.style.MyAlertDialogStyle);

        String[] tourdatas = tour.split("\n");
        //date, place, user

        builder.setTitle(tourdatas[1]);
        builder.setIcon(R.drawable.together_tour_clock);
        String datas = "Jelentkezni szeretnél?\n\nAkik már jelentkeztek:\n";

        for (String name : participants) {
            datas += name + "\n";
        }

        builder.setPositiveButton("Jelentkezés a túrára", (dialog, which) -> {
            Database.getInstance().jointour(tourid);
        });

        builder.setNeutralButton("Mégse", (dialog, which) -> {

        });

        builder.setMessage(datas);
        builder.create();
        builder.show();
    }

    public void tourListSet(ArrayList<TogetherAdaper> list) {
        TourAdapter adapter = new TourAdapter(this, R.layout.touradapter, list) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view.findViewById(R.id.textView1);

                tv.setTextColor(Color.parseColor("#FFFFFF"));

                return view;
            }
        };
        this.list.setAdapter(adapter);
    }


    public static TourActivity getInstance() {
        return instance;
    }

}