package com.example.myapplication.Dialogs;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.myapplication.Database.Database;
import com.example.myapplication.R;

public class DialogAddFriend extends AppCompatDialogFragment {
    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);

        final View customLayout = getLayoutInflater().inflate(R.layout.custom_dialog, null);

        builder.setView(customLayout);

        builder.setTitle("Barát hozzáadása");

        builder.setMessage("Kit szeretnél felenni a barátaid közé?");

        builder.setPositiveButton("Küldés", (dialog, which) -> {
            EditText nick = customLayout.findViewById(R.id.text);
            String nickname = String.valueOf(nick.getText());
            //user if exist
            Database.getInstance().existUsername(nickname);
        });

        builder.setNegativeButton("Mégse", (dialog, id) -> {

        });

        return builder.create();
    }


}
