package com.example.myapplication.Dialogs;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.myapplication.Activitys.MainActivitys.MapActivity;
import com.example.myapplication.R;

public class DialogGPS extends AppCompatDialogFragment {

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        builder.setTitle("Figyelmeztetés");
        builder.setMessage("Nincs bekapcsolva a GPS.\nKérem kapcsolja be!");
        builder.setIcon(R.drawable.dialoggps_gpslogo);
        builder.setPositiveButton("GPS bekapcsolása", (dialog, which) -> MapActivity.getInstance().GPS());
        return builder.create();
    }
}
