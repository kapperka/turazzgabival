package com.example.myapplication.Dialogs;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.myapplication.Database.Database;
import com.example.myapplication.R;

public class DialogNickname extends AppCompatDialogFragment {
    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);

        final View customLayout = getLayoutInflater().inflate(R.layout.custom_dialog, null);

        builder.setView(customLayout);

        builder.setTitle("Becenév.");

        builder.setMessage("Nincs Beceneved!\n Írd be, higy mi legyen a beceneved.");

        //builder.setIcon();

        builder.setPositiveButton("Becenév beállítása", (dialog, which) -> {
            EditText nick = customLayout.findViewById(R.id.text);
            Toast.makeText(getActivity(), nick.getText(), Toast.LENGTH_SHORT).show();
            String nickname = String.valueOf(nick.getText());
            Database.getInstance().setUsername(nickname);
        });

        return builder.create();
    }


}
