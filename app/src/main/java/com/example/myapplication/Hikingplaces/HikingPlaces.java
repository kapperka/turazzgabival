package com.example.myapplication.Hikingplaces;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Activitys.MainActivitys.MapActivity;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mapbox.geojson.Point;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class HikingPlaces extends AppCompatActivity implements Serializable {
    private Context context;
    private ArrayList<HikingPlace> hikingplaces = new ArrayList<>();

    public HikingPlaces(Context current) {
        //Log.d("HikingPlaces", "Adatok lekérése");
        try {
            this.context = current;
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference file = storage.getReferenceFromUrl("gs://turazzgabival.appspot.com/tura2.csv");
            //Log.d("HikingPlaces", "Adatbázis");
            final long ONE_MEGABYTE = 1024000L * 1024000;
            file.getBytes(ONE_MEGABYTE).addOnSuccessListener(bytes -> {
                //Log.d("HikingPlaces", "Sikeres belépés");
                String s = new String(bytes);
                String[] ss = s.split("\n");
                for (String d : ss) {
                    String[] all = d.split(";");
                    HikingPlace place = new HikingPlace(all[0], all[1], all[2], all[3], all[4], all[7], all[6], all[5], coordinates(all[8]));
                    hikingplaces.add(place);
                }
                MapActivity.getInstance().routes(hikingplaces);
                MapActivity.getInstance().setHikingplaces_2(hikingplaces);
                MapActivity.getInstance().jump();
            }).addOnFailureListener(exception -> {

            });

        } catch (Exception ignored) {

        }
    }

    private List<Point> coordinates(String rawcoor) {
        List<Point> routeCoordinates = new ArrayList<>();
        String[] coordinates = rawcoor.split("],");
        for (int i = 0; i < coordinates.length; i++) {
            coordinates[i] = coordinates[i].replace("]", "");
            coordinates[i] = coordinates[i].replace("[", "");
            String[] coor = coordinates[i].split(",");
            routeCoordinates.add(Point.fromLngLat(
                    Double.parseDouble(coor[1]),
                    Double.parseDouble(coor[0])));
        }
        return routeCoordinates;
    }


    public List<Point> getselectedcoordinates(String selected) {
        List<Point> points = null;
        for (HikingPlace place : hikingplaces) {
            if (place.getName().equals(selected)) {
                points = place.getCoordinates();
            }
        }
        return points;
    }

    public List<com.mapbox.geojson.Point> getcoor(String name) {
        List<Point> routeCoordinates = new ArrayList<>();
        for (HikingPlace place : hikingplaces) {
            if (name.equals(place.getName())) {
                List<com.mapbox.geojson.Point> coordinates = place.getCoordinates();

                routeCoordinates.add(coordinates.get(0));
            }
        }
        return routeCoordinates;
    }

}
