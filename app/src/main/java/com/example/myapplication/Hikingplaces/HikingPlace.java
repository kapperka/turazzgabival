package com.example.myapplication.Hikingplaces;


import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.util.ArrayUtils;
import com.mapbox.geojson.Point;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class HikingPlace extends AppCompatActivity implements Serializable {
    private String name;
    private String area;
    private String difficulty;
    private String difference;
    private String distance;
    private String distancecategory;
    private String[] attributes;
    private String diferencecategory;
    private List<com.mapbox.geojson.Point> coordinates;


    public HikingPlace(String place, String area, String distancecategory, String difficulty, String diferencecategory, String difference, String distance, String attributes_, List<Point> coordinates) {
        this.name = place;
        this.area = area;
        this.difficulty = difficulty;

        this.distancecategory = distancecategory;
        this.difference = difference;

        this.diferencecategory = diferencecategory;
        this.distance = distance;

        String[] tmp = attributes_.split(",");

        this.attributes = Arrays.stream(tmp).map(String::trim).toArray(String[]::new);
        this.coordinates = coordinates;
    }

    public boolean include(String attribute) {
        return ArrayUtils.contains(attributes, attribute);
    }

}
